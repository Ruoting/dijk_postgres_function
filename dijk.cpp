//============================================================================
// Author      : Ting Lei, tinglei.geog@gmail.com
// Version     :
// Copyright   : all rights reserved
// Description :
//============================================================================

#include <vector>
#include <set>
#include <map>
#include <float.h>

#include <iostream>
#include <fstream>
#include <sstream>

#include "groupby.h"
#include "dijk.h"
#include "edgecost.h"

#include "prettyprint.hpp"


// extern output_t* output_tuples;
extern "C" double dijk_process(input_t * edge_tuples, int length, long src, long dst);
//Palloc can not be used here. We need to malloc space in C.
#include "my_func.h" //transfer C to C-plus



//note: the graph type std::vector<std::vector<sch_edge> > requires that the
//node ids be continuous and starts from 0, i.e. node ids in sch_edge should be indexes
//o.w. graph type should be std::map<int, std::vector<sch_edge> > with log(N) overhead

//============================================================================
// Author      : Ruoting Zheng
// Start       : 2018-5-10
// Update      : 2018-6-3
// Description : Add two functions for dijk processing
//============================================================================

/*****              Load table into structures                *****/

bool load_net_sql(input_t * edge_tuples, int length, graph_t & graph, std::string& err)
{
  graph.clear();
  
  std::string dummy, descriptor, w_type, dept, arrv;
  node_idx_t from, to;
  std::vector<sch_edge> edges;
  size_t arc_id = 0;
  struct sch_edge arc_current;

  for (size_t i = 0; i < length; i++) {

    arc_current.edge_id = edge_tuples[i].edge_id_c;
    arc_id += 1;
    arc_current.from = edge_tuples[i].from_nd_c;
    arc_current.to = edge_tuples[i].t_nd_c;
    arc_current.has_tw = edge_tuples[i].has_tw_c;


    if (! arc_current.has_tw) {
      arc_current.length = edge_tuples[i].length_c;
      arc_current.tw = {};
      edges.emplace_back(arc_current); //add edges w/o tw as undirected edge, i.e. both directions
      arc_current.from = edge_tuples[i].t_nd_c;
      arc_current.to = edge_tuples[i].from_nd_c;
      edges.emplace_back(arc_current);  //add the opposite direction
    }
    else {
      arc_current.length = DBL_MAX;
      // iss >> dept >> arrv;
      
      //strncpy(arc_current -> line_name, name_buf, MAX_NAME_LEN);
      arc_current.tw_size = edge_tuples[i].interval_num;
      if (arc_current.tw_size <= 0) {
        err = "--specifically, time windowed weight empty"; return false;
      }

      arc_current.tw.resize(arc_current.tw_size);
      for (int itmp = 0; itmp < arc_current.tw_size; itmp++) {
          arc_current.tw[itmp].start = edge_tuples[i].sch[itmp].start;
          arc_current.tw[itmp].stop = edge_tuples[i].sch[itmp].stop;
        }
      edges.emplace_back(arc_current); //add tw edges as directed ones
    }//else
  }//for contents

  auto gps = groupBy(edges,[](auto e){ return e.from; });
  //std::cout << gps.size() << "! "; //debug
  //  graph.resize(gps.size()); //v index
  for (auto &g: gps) {
    //std::cout << g[0].from << ", "; //debug
    graph[g[0].from] = g;
  }
  return true;
}



/*****              Called by myfunc.c, processing                *****/

double dijk_process(input_t * edge_tuples, int length, long src, long dst){
  graph_t graph;
  std::string err;
  
  node_costs costs;

  load_net_sql(edge_tuples, length, graph, err);
  double res = dijkstra_sch(graph, src, dst, costs, MAX_TIME, true, 540 , true, 60.0);

  get_space(costs.size());
    
  std::vector<std::pair<double,edge_idx_t>>::iterator iter;  
  int i = 0;
  for (iter=costs.begin();iter!=costs.end();iter++)  {   
    set_tuple(i, (*iter).first, (long)(*iter).second);
    i ++;
  }  

  return costs.size();
}



//============================================================================
// 
//                    Origin Functions
// 
//============================================================================

double dijkstra(const graph_t& graph, node_idx_t source, node_idx_t target, node_costs& min_distance) {
  //std::vector<double> min_distance( graph.size(), DBL_MAX );
    min_distance.resize( graph.size(), {DBL_MAX,-1} );
    min_distance[ source ] = {0,-1};
    std::set< std::pair<double,node_idx_t> > active_vertices;
    active_vertices.insert( {0,source} );
        
    while (!active_vertices.empty()) {
        node_idx_t where = active_vertices.begin()->second;
        if (where == target) return min_distance[where].first;
        active_vertices.erase( active_vertices.begin());     
        //for (auto ed : graph[where]) //only w/ std::vector & vertex index
        for (auto ed : graph.at(where)) {
            if (min_distance[ed.to].first > min_distance[where].first + ed.length) {
                active_vertices.erase( { min_distance[ed.to].first, ed.to } );
                min_distance[ed.to] = {min_distance[where].first + ed.length, ed.edge_id};
                active_vertices.insert( { min_distance[ed.to].first, ed.to } );
            }
        }
    }
    return DBL_MAX;
}

double dijkstra_sch(const graph_t& graph, node_idx_t source, node_idx_t target, node_costs& min_distance
                    , double maxlen, int use_tw, double start_time, bool is_dept_time,double pspeed) {
  //std::vector<double> min_distance( graph.size(), DBL_MAX );
	  //std::vector<double> min_distance( graph.size(), DBL_MAX );
	min_distance.resize(graph.size(), { DBL_MAX, -1 });
	min_distance[ source ] = {0,-1};
	std::set<std::pair<double, node_idx_t> > active_vertices;
	active_vertices.insert( { 0, source });
        
    while (!active_vertices.empty()) {
        node_idx_t where = active_vertices.begin()->second;
        if (where == target) return min_distance[where].first;
        active_vertices.erase( active_vertices.begin());     
        double dist_new;
        //for (auto ed : graph[where]) //only w/ std::vector & vertex index
        for (auto ed : graph.at(where)) {
          if (use_tw) {
            dist_new = propagate_cost(min_distance,start_time, ed.from, ed, is_dept_time,pspeed);
          }
          else {
            dist_new = min_distance[where].first + ed.length;
          }

          if (min_distance[ed.to].first > dist_new) {
            active_vertices.erase( { min_distance[ed.to].first, ed.to } );
            min_distance[ed.to] = { dist_new, ed.edge_id};
            active_vertices.insert( { min_distance[ed.to].first, ed.to } );
          }
        }
    }
    return DBL_MAX;
}  
