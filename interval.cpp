/* Ting Lei: functions for locating the next interval from a schedule
 */
#include <stdio.h>
#include "interval.h"
#include <assert.h>
#include <vector>

/* find a time interval that is next to time */

int lbound_index(const std::vector<interval_cpp>& intv_arr,int n, double time, int is_forward)
{
	if (n <= 0) {printf ("empty interval array in lbound_index.\n"); exit(1);}
	int i,j,k;
	i = 0, j = n - 1;
	/* handle boundary conditions first */
	if (is_forward && time > intv_arr[j].start)
		return -1;
	if (!is_forward && time < intv_arr[i].stop)
		return -1;

	while (i < j) /* binary search for the intv that contains
		or immediately precedes -time- */
	{
		k = (i+j)>>1;  /* /2; does not handle negatives correctly */

		if (is_forward) {
			if (time <= intv_arr[k].start) /* intv k is possible */
				j = k;
			else
				i = k + 1;
			if (time == intv_arr[k].start)
				return k;
		}
		else
		{
			k = k + 1;
			if (time >= intv_arr[k].stop)
				i = k;
			else
				j = k - 1;
			if (time == intv_arr[k].stop)
				return k;
		}
		/* debug
		printf("(i,j,k,n,time,is_forward)=(%d,%d,%d,%d,%lf,%d)\n",i,j,k,n,time,is_forward);
		*/
	}
	assert(i == j);
	return i;

}


auto lbound(const std::vector<interval_cpp>& intv_arr,int n, double time, int is_forward) -> decltype(intv_arr.begin())
{
	int idx = lbound_index(intv_arr,n,time,is_forward);
	if (idx < 0 || idx >=n)
            return intv_arr.end();
	return (intv_arr.begin() + idx);
}
