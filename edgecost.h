/* Ting Lei: functions for locating the next interval from a schedule
 */
#ifndef __edgecost_h_
#define __edgecost_h_

#include "dijk.h"

// calculate arc cost based on arc weight type and if necessary, current time/distance
double propagate_cost(const node_costs& min_distance, double start_time, node_idx_t node_from, const sch_edge& a, bool is_dept_time,double pspeed);

#endif /* __edgecost_h_ */
