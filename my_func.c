//============================================================================
// Author      : Ruoting Zheng
// Start       : 2018-5-10
// Update      : 2018-6-3
// Description :
//============================================================================

#include <postgres.h>
#include <fmgr.h>
// #include <utils/geo_decls.h>
#include <funcapi.h>
#include <executor/spi.h>

#include <string.h>
#include "input_t.h" //for input table
#include "output_t.h"   //for output

#include "my_func.h" //transfer C to C-plus

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

input_t* edge_tuples = NULL;   //for test only
output_t* output_tuples = NULL;    //for test only

extern int readtable(); //from edge_input

//from dijk.cpp
extern double dijk_process(input_t * edge_tuples, int length, long src, long dst);


PG_FUNCTION_INFO_V1(dijk_c);

Datum dijk_c(PG_FUNCTION_ARGS)
{
    FuncCallContext     *funcctx;
    int                  call_cntr;
    int                  max_calls;
    TupleDesc            tupdesc;
    AttInMetadata       *attinmeta;
    char* array_str; 
    int string_len;

    int count;  //number of input table
    double result_distance;
    int64 src;
    int64 dst;

    /* stuff done only on the first call of the function */
    if (SRF_IS_FIRSTCALL())
    {
        MemoryContext   oldcontext;

        /* create a function context for cross-call persistence */
        funcctx = SRF_FIRSTCALL_INIT();

        /* switch to memory context appropriate for multiple function calls */
        oldcontext = MemoryContextSwitchTo(funcctx->multi_call_memory_ctx);

    	src = PG_GETARG_INT64(0);
    	dst = PG_GETARG_INT64(1);

        /* total number of tuples to be returned */
        count = readtable();
        
        /* C-plus */
        int result_length = dijk_process(edge_tuples, count, src, dst);
        funcctx->max_calls = result_length;
        // funcctx->max_calls = count;

        /* Free the input structures  */
        if (edge_tuples != NULL){
            for (int i=0; i<count; i++){
                if (edge_tuples[i].sch != NULL){
                    SPI_pfree(edge_tuples[i].sch);
                }
            }
            SPI_pfree(edge_tuples); 
        }

        // funcctx->max_calls = count;
        if (funcctx->max_calls == 0){
            SRF_RETURN_DONE(funcctx);
        }

        /* Build a tuple descriptor for our result type */
        if (get_call_result_type(fcinfo, NULL, &tupdesc) != TYPEFUNC_COMPOSITE)
            ereport(ERROR,
                    (errcode(ERRCODE_FEATURE_NOT_SUPPORTED),
                     errmsg("function returning record called in context "
                            "that cannot accept type record")));

        /*
         * generate attribute metadata needed later to produce tuples from raw
         * C strings
         */
        attinmeta = TupleDescGetAttInMetadata(tupdesc);
        funcctx->attinmeta = attinmeta;

        MemoryContextSwitchTo(oldcontext);

    }

    /* stuff done on every call of the function */
    funcctx = SRF_PERCALL_SETUP();

    call_cntr = funcctx->call_cntr;
    max_calls = funcctx->max_calls;
    attinmeta = funcctx->attinmeta;
    // result_tuples = (body_t *)funcctx->user_fctx;
 

    if (call_cntr < max_calls)    /* do when there is more left to send */
    {
        char       **values;
        HeapTuple    tuple;
        Datum        result;

        /*
         * Prepare a values array for building the returned tuple.
         * This should be an array of C strings which will
         * be processed later by the type input functions.
         */
        values = (char **) palloc(2 * sizeof(char *));
        values[0] = (char *) palloc(20 * sizeof(char));
        values[1] = (char *) palloc(20 * sizeof(char));

        snprintf(values[0], 20, "%ld", output_tuples[call_cntr].edge_id);
        snprintf(values[1], 20, "%lf", output_tuples[call_cntr].distance);

        /* build a tuple */
        tuple = BuildTupleFromCStrings(attinmeta, values);

        /* make the tuple into a datum */
        result = HeapTupleGetDatum(tuple);

        /* clean up (this is not really necessary) */
        pfree(values[0]);
        pfree(values[1]);
        pfree(values);

        SRF_RETURN_NEXT(funcctx, result);
    }
    else    /* do when there is no more left */
    {
    	// pfree(funcctx->user_fctx);
        if (output_tuples != NULL)
            pfree(output_tuples); //only for test
        SRF_RETURN_DONE(funcctx);
    }
}


// Call by Cplus functions
void get_space(int size){
    output_tuples = (output_t*)palloc(size * sizeof(output_t));
}

void set_tuple(int i, double distance, long edge_id){
    output_tuples[i].distance = distance;
    output_tuples[i].edge_id = edge_id;
}