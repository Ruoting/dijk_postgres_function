//============================================================================
// Author      : Ruoting Zheng
// Start       : 2018-5-10
// Update      : 2018-6-3
// Description : Time data transfer
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "interval_t.h"



/********************************************************/
/* get max length of the interval array                 */
/********************************************************/
short max_interval_slot(char *time_list1, char *time_list2)
{
	short	a, b;

	a = strlen(time_list1);
	b = strlen(time_list2);
	if (b > a)
		a = b;
	return(a / 9 + 2);
}


/********************************************************/
/* Find positon of the start of time string             */
/********************************************************/
static char *valid_pos(char *str)
{
	char	*p;
	short	find = 0;

	for (p = str; *p; p++) {
		if (*p == '{') continue;
		if (*p == ' ') continue;
		if (*p == ',') continue;
		if (*p == '}') break;
		find = 1;
		break;
	}
	return(find ? p : NULL);
}

/********************************************************/
/* Get value of minutes from a time string              */
/********************************************************/
static long get_minutes(char *str)
{
	char	aa[3];
	long	mm;

	// printf("str_s: ");
	// for (int i = 0; i < 8; i++)
	// 	printf("%c", str[i]);
	// printf("\n");

	aa[0] = str[0];
	aa[1] = str[1];
	aa[2] = 0;
	if (aa[0] < '0' || aa[0] > '2')
		return(-1L);
	if (aa[1] < '0' || aa[1] > '9')
		return(-2L);
	mm = atol(aa);

	if (str[2] != ':')
		return(-3L);
	mm *= 60;

	aa[0] = str[3];
	aa[1] = str[4];
	aa[2] = 0;
	if (aa[0] < '0' || aa[0] > '6')
		return(-4L);
	if (aa[1] < '0' || aa[1] > '9')
		return(-5L);
	mm += atol(aa);

	if (str[5] != ':')
		return(-6L);
	if (str[6] != '0')
		return(-6L);
	if (str[7] != '0')
		return(-7L);

	return(mm);
}

/********************************************************/
/* Fetch interval array from field sets                 */
/********************************************************/
short get_interval(interval_t *intvl_array, short array_len, char *time_list1, char *time_list2)
{
	short	i;
	long	mm;
	char	*str1, *str2;

	str1 = time_list1;
	str2 = time_list2;
	// printf("%s\n", str1);
	// printf("%s\n", str2);

	for (i = 0; i < array_len; i++) {
		if ((str1 = valid_pos(str1)) == NULL){
			printf("string1valid\n");
			break;
		}
		if ((str2 = valid_pos(str2)) == NULL){
			printf("string2valid\n");
			break;
		}
		if ((mm = get_minutes(str1)) < 0L){
			printf("getmin1, %ld\n", mm);
			break;
		}
		intvl_array[i].start = mm;
		if ((mm = get_minutes(str2)) < 0L){
			printf("getmin2\n");
			break;
		}
		intvl_array[i].stop = mm;
		str1 += 8;
		str2 += 8;				
	}
	return(i);
}

// int main (){
// 	char * time_list1 = "{09:15:00,09:45:00,10:15:00,10:25:00,10:35:00,10:45:00,10:55:00,11:05:00,11:15:00,11:25:00,11:35:00,11:45:00,11:55:00,12:05:00,12:15:00,12:25:00,12:35:00,12:45:00,12:55:00,13:05:00,13:15:00,13:25:00,13:35:00,13:45:00,13:55:00,14:05:00,14:15:00,14:25:00,14:35:00,14:45:00,14:55:00,15:05:00,15:15:00,15:25:00,15:35:00,15:45:00,15:55:00,16:05:00,16:15:00,16:25:00,16:35:00,16:45:00,16:55:00,17:05:00,17:15:00,17:25:00,17:35:00,17:45:00,17:55:00,18:05:00,18:15:00}\x02\x08ѳ\x02\x08\x17w\x18\x12]:<\x02\x08_\x02\x08\x02\x08/\x02\x08uG\x02\x08+";
// 	char * time_list2 = "{09:16:00,09:46:00,10:16:00,10:26:00,10:36:00,10:46:00,10:56:00,11:06:00,11:16:00,11:26:00,11:36:00,11:46:00,11:56:00,12:06:00,12:16:00,12:26:00,12:36:00,12:46:00,12:56:00,13:06:00,13:16:00,13:26:00,13:36:00,13:46:00,13:56:00,14:06:00,14:16:00,14:26:00,14:36:00,14:46:00,14:56:00,15:06:00,15:16:00,15:26:00,15:36:00,15:46:00,15:56:00,16:06:00,16:16:00,16:26:00,16:36:00,16:46:00,16:56:00,17:06:00,17:16:00,17:26:00,17:36:00,17:46:00,17:56:00,18:06:00,18:16:00}\x08|\x17\x05\x12x(\x02\x08\x08L\x02\x08Nap\x02\x08$\x02\x08\x02\x08 \x02\x08fn\x02\x081#\x12\x08";
// 	short len, num;
// 	interval * xxx;

	// len = max_interval_slot(time_list1, time_list2);
	// xxx = (interval *) malloc (len * sizeof(interval));

	// num = get_interval(xxx, len, time_list1, time_list2);
	// for(int i=0; i<num; i ++){
	// 	printf("Time %d,start:%ld,stop:%ld\n", i,xxx[i].start, xxx[i].stop);
	// }
	// free(xxx);
// 	printf("max:%d, number:%d", len, num );
// 	return (0);
// }

