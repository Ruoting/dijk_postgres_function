/* Ting Lei: functions for locating the next interval from a schedule
 */
#ifndef __interval_h_
#define __interval_h_

#include <stdlib.h>
#include <vector>

typedef struct {
	long start;
	long stop;
} interval_cpp;

int lbound_index(const std::vector<interval_cpp>& intv_arr,int n, double time, int is_forward);
auto lbound(const std::vector<interval_cpp>& intv_arr,int n, double time, int is_forward) -> decltype(intv_arr.begin());

#endif /* __interval_h_ */
