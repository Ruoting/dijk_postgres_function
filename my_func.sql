/*
5-21-2018 Tryout 
*/

CREATE OR REPLACE FUNCTION dijkc(
    src BIGINT, 
    dst BIGINT, 
    -- hello_sql TEXT,
    -- start_vids ANYARRAY,
    -- end_vids ANYARRAY,
    -- directed BOOLEAN DEFAULT true,
    -- only_cost BOOLEAN DEFAULT false,
    -- normal BOOLEAN DEFAULT true,

    OUT edge_id BIGINT,
    OUT distance double precision
    -- OUT from_nd BIGINT, 
    -- OUT t_nd BIGINT, 
    -- OUT has_tw boolean, 
    -- OUT length double precision,
    -- OUT dept TEXT,
    -- OUT arrv TEXT
    )
-- RETURNS SETOF RECORD AS
RETURNS SETOF RECORD AS
'/home/ruoting/dijk_function/my_func.so', 'dijk_c'
LANGUAGE c VOLATILE;