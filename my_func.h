/* file my_func.h */
#ifndef MYFUNC_H
#define MYFUNC_H
 
#ifdef __cplusplus
extern "C" {
#endif
 
void get_space(int size);
void set_tuple(int i, double distance, long edge_id);

       
#ifdef __cplusplus
}
#endif
 
#endif /* MYFUNC_H */