//============================================================================
// Author      : Ting Lei, tinglei.geog@gmail.com
// Version     :
// Copyright   : all rights reserved
// Description :
//============================================================================

#include <vector>

#include <iostream>

#include <algorithm> //partition

template <typename T, typename projection>
auto groupBy(std::vector<T> a, projection p) {
  auto from = a.begin();
  std::vector<std::vector<T> > gs;
  while (from != a.end()) {
    auto from_new = std::partition(from + 1,a.end(),[&](const T x){return p(x) == p(*from);});
    gs.emplace_back(std::vector<T>(from, from_new));
    from = from_new;
  }
  return gs;
}


// int main() {
//   std::vector<int> xs = {1,1,2,3,3,3,1,5,5};
//   auto gs = groupBy(xs, [](auto x){return x;});
//   for (auto &g: gs) {
//     for (auto &e: g) 
//       std::cout << e << " " ;
//     std::cout << "\n";
//   }
// }



