//============================================================================
// Author      : Ruoting Zheng
// Start       : 2018-5-10
// Update      : 2018-6-3
// Description : For read input data from table
//============================================================================
#include "interval_t.h"
typedef struct {
	int64_t edge_id_c;
	int64_t from_nd_c; 
	int64_t t_nd_c;
	bool has_tw_c;
	double length_c;
	
	short interval_num;
	interval_t * sch;
} input_t;


