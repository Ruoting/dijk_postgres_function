PGS=/usr/include/postgresql/10/server

my_fo: my_func.o edge_input.o interval.o interval_t.o edgecost.o dijk.o
	gcc -shared -o my_func.so my_func.o edge_input.o interval.o interval_t.o edgecost.o dijk.o

my_func.o: my_func.c input_t.h output_t.h my_func.h
	gcc -fPIC -I $(PGS) -c my_func.c 

edge_input.o: edge_input.c input_t.h
	gcc -fPIC -I $(PGS) -c edge_input.c

interval_t.o: interval_t.c interval_t.h
	gcc -fPIC -c interval_t.c

interval.o: interval.cpp
	g++ -fPIC -c interval.cpp -o interval.o

edgecost.o: edgecost.cpp input_t.h dijk.h 
	g++ -fPIC -c edgecost.cpp -o edgecost.o

dijk.o: dijk.cpp dijk.h output_t.h input_t.h my_func.h
	g++ -fPIC -I $(PGS) -c dijk.cpp -o dijk.o
