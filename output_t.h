//============================================================================
// Author      : Ruoting Zheng
// Start       : 2018-5-10
// Update      : 2018-6-3
// Description : Defined structure for output
//============================================================================
typedef struct {
	double distance;
	long edge_id;
} output_t;