// Ting Lei: functions calculating costs of the next time

#include <stdio.h>
#include "edgecost.h"
#include <assert.h>
#include <algorithm>

#include "dijk.h"
using namespace std;

// statistical variables
long n_scans = 0;
long n_impr = 0;

// pedestrian speed
double g_pspeed;
void set_ped_speed(double sp) {
	g_pspeed = sp;
	if (sp <= 0)
		g_pspeed = (4500 / 0.3048) / 60;  //default to 5km/h (assumes map unit is feet)
}

double get_ped_speed() {
	if (g_pspeed <= 0)
		g_pspeed = (4500 / 0.3048) / 60;  //default to 5km/h (assumes map unit is feet)
	return g_pspeed;
}

void print_arc(sch_edge a) {
  //printf("Arc %s: %d -> %d  , tw_size = %d, tw = \n", //a->line_name
  printf("tw_size = %d" ,a.tw_size); //,a->tail_nid, a->head_nid);
  for (int itmp =0; itmp< a.tw_size; itmp++)
    printf("(%ld,%ld)", a.tw[itmp].start, a.tw[itmp].stop);
}
// calculate arc cost based on arc weight type and if necessary, current time/distance
double propagate_cost(const node_costs& min_distance, double start_time, node_idx_t node_from, const sch_edge& a, bool is_dept_time,double pspeed)
{
	const double switch_penalty = 6;  //6 min penalty for switching lines //disable for now
	double dist_from = min_distance.at(node_from).first; //node_from.dist;
	/* return a->len; debug*/
	if (! a.has_tw)
		return min(dist_from + a.length/pspeed,MAX_TIME);

	/* do binary search in the time windows*/
	int idx = lbound_index(a.tw, a.tw_size, dist_from + start_time, is_dept_time);

	if (idx < 0 || idx >= a.tw_size){ //!debug
		//printf(	"Empty intervals in propagate_cost -- ");
		//print_arc(a);
		//printf("start_time = %lf, dist_from = %lf, interval selected = %d,is_dept_time = %d\n", start_time, dist_from, idx, is_dept_time);
	}//!debug

	auto intv = lbound(a.tw,a.tw_size,dist_from + start_time,is_dept_time);
	if (intv == a.tw.end())
		return MAX_TIME;
	double dist;
	if (is_dept_time)
		dist = intv->stop - (dist_from + start_time);
	else
		dist = (dist_from + start_time) - intv->start;

	if (idx >=0 && idx < a.tw_size) { //!debug
			//printf("returning TW weight: %lf for arc %ld, dist_from=%lf\n", dist,(long)a, dist_from);
	}

	if (dist < 0) { //debug
		printf("negative distance at arc -- ");
		print_arc(a);
		printf("start_time = %lf, dist_from = %lf, interval selected = %d,is_dept_time = %d\n", start_time, dist_from, idx, is_dept_time);
		return MAX_TIME;
	}

//        bool bSwitchLine = (a->tail->parent_arc != NULL) && (
//        		string(a->tail->parent_arc->line_name) != string(a->line_name)
//        		&& !a->tail->parent_arc->tw_size > 0 && !a->tw_size > 0);
// 
//        return min(dist + dist_from + (bSwitchLine? switch_penalty : 0),MAX_TIME);
        return min(dist + dist_from ,MAX_TIME);
}
