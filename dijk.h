#ifndef  __dijk_h_
#define __dijk_h_

#include <vector>
#include <set>
#include <map>

#include "interval.h"
#include "input_t.h"
#include "output_t.h"

typedef long int node_idx_t;
typedef long int edge_idx_t;
struct edge { node_idx_t to; double length; };
struct sch_edge { edge_idx_t edge_id; node_idx_t from; node_idx_t to; double length; bool has_tw; int tw_size; std::vector<interval_cpp> tw;};
typedef std::map<node_idx_t, std::vector<sch_edge> > graph_t; //typedef std::vector<std::vector<sch_edge> > graph_t;
typedef std::vector<std::pair<double,edge_idx_t> > node_costs;
//typedef std::map<node_idx_t, double> node_costs;

#define  MAX_TIME  120.0


// bool load_net_gr(std::string input_net_gr, graph_t & graph, std::string& err);

// bool gr_to_csv(const graph_t & graph, std::string csv_file, std::string& err);

// double dijkstra(const graph_t& graph, node_idx_t source, node_idx_t target, node_costs& min_distance);
bool load_net_sql(input_t * edge_tuples, int length, graph_t & graph, std::string& err);

double dijkstra_sch(const graph_t& graph, node_idx_t source, node_idx_t target, node_costs& min_distance
	                    , double maxlen, int use_tw, double start_time, bool is_dept_time,double pspeed);

#endif //__dijk_h_
