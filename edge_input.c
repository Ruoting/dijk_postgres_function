//============================================================================
// Author      : Ruoting Zheng
// Start       : 2018-5-10
// Update      : 2018-6-3
// Description : Read data from database server
//============================================================================
#include <postgres.h>
#include <executor/spi.h>
#include <utils/builtins.h>

#include <string.h>

#include "input_t.h"



// Table In:
// edge_id BIGINT, 
// from_nd BIGINT, 
// t_nd BIGINT, 
// has_tw boolean, 
// length FLOAT8, 

// dept TIME[], 
// arrv TIME []

extern input_t* edge_tuples;

/*******  from interval.c *************/
extern short max_interval_slot(char *, char *);
extern short get_interval(interval_t *, short , char *, char *);

int readtable() 
{
		// input_t* edge_tuples;

    	char* command = "SELECT * FROM mini_net;"; 
    	int count;
    	bool isnull;
    	char* array_str1;
    	char* array_str2; 
    	int string_len;

        // SPI_connect();
        int SPIcode = SPI_connect();

	    if (SPIcode  != SPI_OK_CONNECT) {
	        elog(ERROR, "Couldn't open a connection to SPI");
	        SPI_finish(); 
	        // return(55);
	    }

	    SPIcode = SPI_execute(command, true, 0);
       	if (SPIcode  != SPI_OK_SELECT) {
	        elog(ERROR, "Couldn't open a connection to SPI");
	        SPI_finish(); 
	        // return(233);
	    }

       	count = SPI_processed;
        if (count > 0)
        	edge_tuples = (input_t *) SPI_palloc(count * sizeof(input_t));

        TupleDesc tupdesc = SPI_tuptable->tupdesc;

        for (int i = 0; i < count; i ++){
        	HeapTuple tuple = SPI_tuptable->vals[i];
        	
        	edge_tuples[i].edge_id_c = DatumGetInt64(SPI_getbinval(tuple, tupdesc, 1, &isnull));
			edge_tuples[i].from_nd_c = DatumGetInt64(SPI_getbinval(tuple, tupdesc, 2, &isnull)); 
			edge_tuples[i].t_nd_c = DatumGetInt64(SPI_getbinval(tuple, tupdesc, 3, &isnull));
			edge_tuples[i].has_tw_c = DatumGetBool(SPI_getbinval(tuple, tupdesc, 4, &isnull));
			edge_tuples[i].length_c = DatumGetFloat8(SPI_getbinval(tuple, tupdesc, 5, &isnull));
			
			if (edge_tuples[i].has_tw_c){
				array_str1 = SPI_getvalue(tuple, tupdesc, 6);
				array_str2 = SPI_getvalue(tuple, tupdesc, 7);
				int len;
				len = max_interval_slot(array_str1, array_str2);
				edge_tuples[i].sch = (interval_t *) SPI_palloc (len * sizeof(interval_t));
				edge_tuples[i].interval_num = get_interval(edge_tuples[i].sch, len, array_str1, array_str2);
			}else
				edge_tuples[i].sch = NULL;
					
        }
      

        SPI_finish();  

        return (count);
}
       
	                   